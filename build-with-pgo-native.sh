#!/bin/sh
set -e

EXTRA_RUSTFLAGS="${EXTRA_RUSTFLAGS:--C target-cpu=native}"

tmpdir=$(mktemp -d)
rm -rf /tmp/pgo-data

RUSTFLAGS="-Cprofile-generate=$tmpdir $EXTRA_RUSTFLAGS" cargo build --release --target=x86_64-unknown-linux-gnu

./target/x86_64-unknown-linux-gnu/release/polycubes2 -n 11

llvm-profdata merge -o "$tmpdir/merged.profdata" "$tmpdir"

RUSTFLAGS="-Cprofile-use=$tmpdir/merged.profdata $EXTRA_RUSTFLAGS" cargo build --release --target=x86_64-unknown-linux-gnu

rm -rf "$tmpdir"
