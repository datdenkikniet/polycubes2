#![allow(clippy::mut_range_bound)]
#![feature(const_mut_refs)]
#![feature(lazy_cell)]

use std::{cmp::Ordering, ops::Add, sync::atomic, sync::LazyLock};

use crate::cli::Args;
use clap::Parser;
use tinyvec::ArrayVec;

mod cli;

const NUM_DIMS: usize = 3;
const NUM_NEIGHBORS: usize = 6;
const NUM_ROTATIONS_3D: usize = 24;
const NUM_ROTATIONS_2D: usize = 4;
const BORDER_PADDING: usize = 2;
const NOT_INSERTED: i32 = -1;
const NUDGE_BASED_ON_ROTATION: [[Coord3DElemType; 6]; 3] = [
    [-1, 0, 1, 0, 0, 0],
    [0, 1, 0, -1, 0, 0],
    [0, 0, 0, 0, 1, -1],
];
type AllNudgeType = [[[Coord3DElemType; NUM_NEIGHBORS]; NUM_DIMS]; NUM_ROTATIONS_3D];

const ALL_NUDGES: AllNudgeType = generate_all_the_nudges();

const fn get_j_column<T: Copy, const N1: usize, const N2: usize>(
    array: &[[T; N1]; N2],
    j: usize,
) -> [T; N2] {
    let mut result = [array[0][0]; N2];
    let mut i = 0;
    while i < N2 {
        result[i] = array[i][j];
        i += 1;
    }
    result
}

const fn is_all_zeros(value: &[Coord3DElemType]) -> bool {
    let len = value.len();
    let mut i = 0;
    while i < len {
        if value[i] != 0 {
            return false;
        }
        i += 1;
    }
    true
}

const fn cross_prod_3d(
    a: &[Coord3DElemType],
    b: &[Coord3DElemType],
) -> [Coord3DElemType; NUM_DIMS] {
    let mut c = [0; NUM_DIMS];
    let mut k = 0;
    while k < NUM_DIMS {
        let mut elements = [0; 4];
        let mut cur_index = 0;

        let mut i = 0;
        while i < NUM_DIMS {
            if i == k {
                i += 1;
                continue;
            }
            elements[cur_index] = a[i];
            cur_index += 1;
            elements[cur_index] = b[i];
            cur_index += 1;

            i += 1;
        }
        c[k] = elements[0] * elements[3] - elements[1] * elements[2];
        if k == 1 {
            c[k] *= -1;
        }

        k += 1;
    }

    c
}

const fn generate_all_the_nudges() -> AllNudgeType {
    let mut all_nudges = [[[0; NUM_NEIGHBORS]; NUM_DIMS]; NUM_ROTATIONS_3D];

    let mut i = 0;
    while i < NUM_NEIGHBORS {
        let mut j = 0;
        while j < NUM_ROTATIONS_2D {
            let array_to_fill = &mut all_nudges[NUM_ROTATIONS_2D * i + j];
            let mut k = 0;
            while k < NUM_DIMS {
                array_to_fill[k][0] = NUDGE_BASED_ON_ROTATION[k][i];
                k += 1;
            }

            let first_column = get_j_column(array_to_fill, 0);
            let mut num90_degree_vector_found_before_current = 0;
            let mut m = i + 1;
            loop {
                let other_vector = get_j_column(&NUDGE_BASED_ON_ROTATION, m % NUM_NEIGHBORS);

                if !is_all_zeros(&cross_prod_3d(&first_column, &other_vector)) {
                    if num90_degree_vector_found_before_current == j {
                        let mut k = 0;
                        while k < NUM_DIMS {
                            array_to_fill[k][1] = other_vector[k];
                            k += 1;
                        }
                        break;
                    }
                    num90_degree_vector_found_before_current += 1;
                }
                m += 1;
            }

            let mut k = 0;
            while k < NUM_DIMS {
                array_to_fill[k][2] = 0 - array_to_fill[k][0];
                array_to_fill[k][3] = 0 - array_to_fill[k][1];
                k += 1;
            }

            let second_column = get_j_column(array_to_fill, 1);
            let product = cross_prod_3d(&first_column, &second_column);

            let mut k = 0;
            while k < NUM_DIMS {
                array_to_fill[k][4] = 0 - product[k];
                k += 1;
            }

            let mut k = 0;
            while k < NUM_DIMS {
                array_to_fill[k][5] = 0 - array_to_fill[k][4];
                k += 1;
            }

            j += 1;
        }
        i += 1;
    }
    all_nudges
}

const LENGTH_SIDES_FOR_EXERCISE: usize = 3;
const START_INDEX_EACH_DIM: usize = LENGTH_SIDES_FOR_EXERCISE / 2;
const fn setup_neighbors_based_on_neighbors_config_index(
    cubes_used: &mut [[[bool; LENGTH_SIDES_FOR_EXERCISE]; LENGTH_SIDES_FOR_EXERCISE];
             LENGTH_SIDES_FOR_EXERCISE],
    index: usize,
    start_index_each_dim: usize,
) {
    let mut i = 0;
    while i < NUDGE_BASED_ON_ROTATION[0].len() {
        if (index & (1 << (NUDGE_BASED_ON_ROTATION[0].len() - 1 - i))) != 0 {
            cubes_used[(start_index_each_dim as isize + NUDGE_BASED_ON_ROTATION[0][i] as isize)
                as usize][(start_index_each_dim as isize
                + NUDGE_BASED_ON_ROTATION[1][i] as isize) as usize]
                [(start_index_each_dim as isize + NUDGE_BASED_ON_ROTATION[2][i] as isize)
                    as usize] = true;
        }
        i += 1;
    }
}
const NUM_NEIGHBORS_CONFIGS: usize = 2usize.pow(NUM_NEIGHBORS as u32);
type VecOfRotations = ArrayVec<[usize; NUM_ROTATIONS_3D]>;

static DEFAULT_VEC_OF_ROTATION: LazyLock<VecOfRotations> = LazyLock::new(|| {
    let mut v = ArrayVec::new();
    for i in 0..v.capacity() {
        v.push(i)
    }
    v
});

static START_ROTATIONS_TO_CONSIDER: LazyLock<Vec<Vec<Option<VecOfRotations>>>> =
    LazyLock::new(|| {
        let mut start_rotations_to_consider = Vec::with_capacity(NUM_NEIGHBORS_CONFIGS);
        for i in 0..NUM_NEIGHBORS_CONFIGS {
            start_rotations_to_consider.push(Vec::with_capacity(NUM_NEIGHBORS_CONFIGS));
            let mut valid_rotations: VecOfRotations = ArrayVec::new();
            'next_array_element: for j in 0..NUM_NEIGHBORS_CONFIGS {
                let mut cubes_used = [[[false; LENGTH_SIDES_FOR_EXERCISE];
                    LENGTH_SIDES_FOR_EXERCISE];
                    LENGTH_SIDES_FOR_EXERCISE];
                cubes_used[START_INDEX_EACH_DIM][START_INDEX_EACH_DIM][START_INDEX_EACH_DIM] = true;
                setup_neighbors_based_on_neighbors_config_index(
                    &mut cubes_used,
                    j,
                    START_INDEX_EACH_DIM,
                );

                for r in 0..NUM_ROTATIONS_3D {
                    let mut index_for_rotation = 0;
                    for dir_new_cell_add in 0..NUM_NEIGHBORS {
                        let new_i = (START_INDEX_EACH_DIM as Coord3DElemType
                            + ALL_NUDGES[r][0][dir_new_cell_add])
                            as usize;
                        let new_j = (START_INDEX_EACH_DIM as Coord3DElemType
                            + ALL_NUDGES[r][1][dir_new_cell_add])
                            as usize;
                        let new_k = (START_INDEX_EACH_DIM as Coord3DElemType
                            + ALL_NUDGES[r][2][dir_new_cell_add])
                            as usize;
                        if cubes_used[new_i][new_j][new_k] {
                            index_for_rotation += 1 << (NUM_NEIGHBORS - 1 - dir_new_cell_add);
                        }
                    }
                    match index_for_rotation.cmp(&i) {
                        Ordering::Greater => {
                            start_rotations_to_consider[i].push(None);
                            continue 'next_array_element;
                        }
                        Ordering::Less => {}
                        Ordering::Equal => valid_rotations.push(r),
                    }
                }
                start_rotations_to_consider[i].push(Some(valid_rotations.clone()))
            }
        }
        start_rotations_to_consider
    });

type Coord3DElemType = i8;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
struct Coord3D {
    x: Coord3DElemType,
    y: Coord3DElemType,
    z: Coord3DElemType,
}

impl Coord3D {
    fn new(x: Coord3DElemType, y: Coord3DElemType, z: Coord3DElemType) -> Coord3D {
        Coord3D { x, y, z }
    }
}

impl Add for Coord3D {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self::new(self.x + rhs.x, self.y + rhs.y, self.z + rhs.z)
    }
}

/// Shorthand for Coord3D::new, because we use it a lot
fn c(x: Coord3DElemType, y: Coord3DElemType, z: Coord3DElemType) -> Coord3D {
    Coord3D::new(x, y, z)
}

#[derive(Clone, Debug)]
struct Vec3D<T> {
    dims: Coord3D,
    vec: Vec<T>,
}

impl<T: Copy> Vec3D<T> {
    fn new(dims: Coord3D, elem: T) -> Vec3D<T> {
        assert!(dims.x > 0);
        assert!(dims.y > 0);
        assert!(dims.z > 0);
        let len = dims.x as usize * dims.y as usize * dims.z as usize;
        Vec3D {
            dims,
            vec: vec![elem; len],
        }
    }

    #[inline(always)]
    fn coord_to_index(&self, coord: Coord3D) -> usize {
        let dims = self.dims;
        debug_assert!(coord.x < dims.x);
        debug_assert!(coord.y < dims.y);
        debug_assert!(coord.z < dims.z);
        coord.x as usize
            + coord.y as usize * dims.x as usize
            + coord.z as usize * dims.x as usize * dims.y as usize
    }

    fn get(&self, coord: Coord3D) -> T {
        self.vec[self.coord_to_index(coord)]
    }

    fn set(&mut self, coord: Coord3D, value: T) {
        let idx = self.coord_to_index(coord);
        self.vec[idx] = value;
    }
}

fn cant_add_cell_because_of_neighbors(
    cubes_to_develop: &[Option<Coord3D>],
    cubes_used: &Vec3D<bool>,
    cubes_ordering: &Vec3D<i32>,
    cur_ordered_idx_to_use: usize,
    new_i: Coord3DElemType,
    new_j: Coord3DElemType,
    new_k: Coord3DElemType,
) -> bool {
    let neighbors_based_on_rotation = [
        c(new_i + 1, new_j, new_k),
        c(new_i - 1, new_j, new_k),
        c(new_i, new_j + 1, new_k),
        c(new_i, new_j - 1, new_k),
        c(new_i, new_j, new_k + 1),
        c(new_i, new_j, new_k - 1),
    ];

    for coord in neighbors_based_on_rotation {
        if cubes_to_develop[cur_ordered_idx_to_use].unwrap() == coord {
            continue;
        }

        if cubes_used.get(coord) {
            let order_other_cell = cubes_ordering.get(coord);
            if order_other_cell < cur_ordered_idx_to_use as i32 {
                return true;
            }
        }
    }

    false
}

fn clear_cubes_used_in_first_function(
    cubes_to_develop: &[Option<Coord3D>],
    cubes_used_in_first_function: &mut Vec3D<bool>,
    cubes_to_develop_in_first_function: &mut [Option<Coord3D>],
) {
    for i in 0..cubes_to_develop.len() {
        if cubes_to_develop[i].is_none() {
            break;
        }
        cubes_to_develop_in_first_function[i] = None;
        cubes_used_in_first_function.set(cubes_to_develop[i].unwrap(), false);
    }
}

fn get_neignbors_index(point: Coord3D, cubes_used: &Vec3D<bool>) -> usize {
    let v1 = if cubes_used.get(
        point
            + c(
                NUDGE_BASED_ON_ROTATION[0][0],
                NUDGE_BASED_ON_ROTATION[1][0],
                NUDGE_BASED_ON_ROTATION[2][0],
            ),
    ) {
        32
    } else {
        0
    };
    let v2 = if cubes_used.get(
        point
            + c(
                NUDGE_BASED_ON_ROTATION[0][1],
                NUDGE_BASED_ON_ROTATION[1][1],
                NUDGE_BASED_ON_ROTATION[2][1],
            ),
    ) {
        16
    } else {
        0
    };
    let v3 = if cubes_used.get(
        point
            + c(
                NUDGE_BASED_ON_ROTATION[0][2],
                NUDGE_BASED_ON_ROTATION[1][2],
                NUDGE_BASED_ON_ROTATION[2][2],
            ),
    ) {
        8
    } else {
        0
    };
    let v4 = if cubes_used.get(
        point
            + c(
                NUDGE_BASED_ON_ROTATION[0][3],
                NUDGE_BASED_ON_ROTATION[1][3],
                NUDGE_BASED_ON_ROTATION[2][3],
            ),
    ) {
        4
    } else {
        0
    };
    let v5 = if cubes_used.get(
        point
            + c(
                NUDGE_BASED_ON_ROTATION[0][4],
                NUDGE_BASED_ON_ROTATION[1][4],
                NUDGE_BASED_ON_ROTATION[2][4],
            ),
    ) {
        2
    } else {
        0
    };
    let v6 = if cubes_used.get(
        point
            + c(
                NUDGE_BASED_ON_ROTATION[0][5],
                NUDGE_BASED_ON_ROTATION[1][5],
                NUDGE_BASED_ON_ROTATION[2][5],
            ),
    ) {
        1
    } else {
        0
    };
    v1 + v2 + v3 + v4 + v5 + v6
}

fn is_first_sight_of_shape(
    cubes_to_develop: &[Option<Coord3D>],
    cubes_used: &Vec3D<bool>,
    num_cells_used_depth: usize,
) -> bool {
    let mut array_standard = vec![0; num_cells_used_depth];
    let mut num = 0;
    let mut min_index_to_use = 0;
    let mut min_rotation = -1;
    let mut cubes_to_develop_in_first_function: Vec<Option<Coord3D>> =
        vec![None; cubes_to_develop.len()];
    let mut cubes_used_in_first_function = Vec3D::new(cubes_used.dims, false);

    cubes_to_develop_in_first_function[0] = cubes_to_develop[0];
    cubes_used_in_first_function.set(cubes_to_develop[0].unwrap(), true);

    'next_cell_insert: for j in 0..(num_cells_used_depth - 1) {
        for cur_ordered_idx_to_use in min_index_to_use..cubes_to_develop.len() {
            if cubes_to_develop[cur_ordered_idx_to_use].is_none() {
                break;
            }

            let mut dir_start = 0;

            if cur_ordered_idx_to_use == min_index_to_use {
                dir_start = min_rotation + 1;
            }

            for dir_new_cell_add in (dir_start as usize)..NUM_NEIGHBORS {
                num += 1;

                let new_i = cubes_to_develop[cur_ordered_idx_to_use].unwrap().x
                    + NUDGE_BASED_ON_ROTATION[0][dir_new_cell_add];
                let new_j = cubes_to_develop[cur_ordered_idx_to_use].unwrap().y
                    + NUDGE_BASED_ON_ROTATION[1][dir_new_cell_add];
                let new_k = cubes_to_develop[cur_ordered_idx_to_use].unwrap().z
                    + NUDGE_BASED_ON_ROTATION[2][dir_new_cell_add];
                let new_coord = c(new_i, new_j, new_k);
                if cubes_used.get(new_coord) && !cubes_used_in_first_function.get(new_coord) {
                    array_standard[j] = num;
                    min_index_to_use = cur_ordered_idx_to_use;
                    min_rotation = dir_new_cell_add as i32;
                    cubes_used_in_first_function.set(new_coord, true);

                    continue 'next_cell_insert;
                }
            }
        }
    }

    let idx_root_neigbors = get_neignbors_index(cubes_to_develop[0].unwrap(), &cubes_used);

    for i in 0..num_cells_used_depth {
        let mut list_of_rotations = *DEFAULT_VEC_OF_ROTATION;
        if num_cells_used_depth >= NUM_NEIGHBORS + 1 {
            let idx_node_neighbors = get_neignbors_index(cubes_to_develop[i].unwrap(), &cubes_used);
            if let Some(vec_of_rotations) =
                START_ROTATIONS_TO_CONSIDER[idx_root_neigbors][idx_node_neighbors]
            {
                list_of_rotations = vec_of_rotations;
            } else {
                return false;
            }
        }

        'next_rotation: for &r in list_of_rotations.iter() {
            if i == 0 && r == 0 {
                continue;
            }

            clear_cubes_used_in_first_function(
                cubes_to_develop,
                &mut cubes_used_in_first_function,
                &mut cubes_to_develop_in_first_function,
            );

            min_index_to_use = 0;
            min_rotation = -1;
            num = 0;
            let cur = cubes_to_develop[i].unwrap();
            cubes_to_develop_in_first_function[0] = Some(cur);
            cubes_used_in_first_function.set(cur, true);
            let mut num_cells_inserted = 1;
            let mut cur_ordered_idx_to_use = min_index_to_use;
            'next_cell_insert: while cur_ordered_idx_to_use
                < cubes_to_develop_in_first_function.len()
            {
                if cubes_to_develop_in_first_function[cur_ordered_idx_to_use].is_none() {
                    break;
                }

                let mut dir_start = 0;
                if cur_ordered_idx_to_use == min_index_to_use {
                    dir_start = min_rotation + 1;
                }

                for dir_new_cell_add in (dir_start as usize)..NUM_NEIGHBORS {
                    num += 1;

                    let new_i = cubes_to_develop_in_first_function[cur_ordered_idx_to_use]
                        .unwrap()
                        .x
                        + ALL_NUDGES[r][0][dir_new_cell_add];
                    let new_j = cubes_to_develop_in_first_function[cur_ordered_idx_to_use]
                        .unwrap()
                        .y
                        + ALL_NUDGES[r][1][dir_new_cell_add];
                    let new_k = cubes_to_develop_in_first_function[cur_ordered_idx_to_use]
                        .unwrap()
                        .z
                        + ALL_NUDGES[r][2][dir_new_cell_add];
                    let new_coord = c(new_i, new_j, new_k);
                    if cubes_used.get(new_coord) && !cubes_used_in_first_function.get(new_coord) {
                        match num.cmp(&array_standard[num_cells_inserted - 1]) {
                            Ordering::Less => return false,
                            Ordering::Greater => {
                                continue 'next_rotation;
                            }
                            Ordering::Equal => {}
                        }
                        min_index_to_use = cur_ordered_idx_to_use;
                        min_rotation = dir_new_cell_add as i32;
                        cubes_used_in_first_function.set(new_coord, true);
                        cubes_to_develop_in_first_function[num_cells_inserted] = Some(new_coord);
                        num_cells_inserted += 1;
                        continue 'next_cell_insert;
                    }
                }
                cur_ordered_idx_to_use += 1;
            }
        }
    }

    true
}

fn do_depth_first_search(
    cubes_to_develop: &[Option<Coord3D>],
    cubes_used: &Vec3D<bool>,
    num_cells_used_depth: usize,
    cubes_ordering: &Vec3D<i32>,
    min_index_to_use: usize,
    min_rotation_to_use: usize,
    max_depth_parallel: u8,
) -> u64 {
    let num_solutions = atomic::AtomicU64::new(0);

    rayon::scope(|s| {
        for cur_ordered_idx_to_use in min_index_to_use..num_cells_used_depth {
            if cubes_to_develop[cur_ordered_idx_to_use].is_none() {
                break;
            }

            for dir_new_cell_add in 0..NUM_NEIGHBORS {
                if cur_ordered_idx_to_use == min_index_to_use
                    && dir_new_cell_add < min_rotation_to_use
                {
                    continue;
                }

                let new_i = cubes_to_develop[cur_ordered_idx_to_use].unwrap().x
                    + NUDGE_BASED_ON_ROTATION[0][dir_new_cell_add];
                let new_j = cubes_to_develop[cur_ordered_idx_to_use].unwrap().y
                    + NUDGE_BASED_ON_ROTATION[1][dir_new_cell_add];
                let new_k = cubes_to_develop[cur_ordered_idx_to_use].unwrap().z
                    + NUDGE_BASED_ON_ROTATION[2][dir_new_cell_add];

                let new_coord = c(new_i, new_j, new_k);

                if cubes_used.get(new_coord) {
                    continue;
                }

                if !cant_add_cell_because_of_neighbors(
                    cubes_to_develop,
                    cubes_used,
                    cubes_ordering,
                    cur_ordered_idx_to_use,
                    new_i,
                    new_j,
                    new_k,
                ) {
                    let mut cubes_used = cubes_used.clone();
                    let mut cubes_to_develop = cubes_to_develop.to_vec();
                    let mut cubes_ordering = cubes_ordering.clone();
                    let num_solutions = &num_solutions;
                    s.spawn(move |_| {
                        cubes_used.set(new_coord, true);
                        cubes_to_develop[num_cells_used_depth] = Some(new_coord);
                        cubes_ordering.set(new_coord, num_cells_used_depth as i32);

                        if is_first_sight_of_shape(
                            &cubes_to_develop,
                            &cubes_used,
                            num_cells_used_depth + 1,
                        ) {
                            let new_solutions =
                                if num_cells_used_depth + 1 == cubes_to_develop.len() {
                                    1
                                } else {
                                    if num_cells_used_depth < max_depth_parallel as usize {
                                        do_depth_first_search(
                                            &cubes_to_develop,
                                            &cubes_used,
                                            num_cells_used_depth + 1,
                                            &cubes_ordering,
                                            cur_ordered_idx_to_use,
                                            dir_new_cell_add,
                                            max_depth_parallel,
                                        )
                                    } else {
                                        do_depth_first_search_serial(
                                            &mut cubes_to_develop,
                                            &mut cubes_used,
                                            num_cells_used_depth + 1,
                                            &mut cubes_ordering,
                                            cur_ordered_idx_to_use,
                                            dir_new_cell_add,
                                        )
                                    }
                                };

                            num_solutions.fetch_add(new_solutions, atomic::Ordering::SeqCst);
                        }

                        cubes_used.set(new_coord, false);
                        cubes_to_develop[num_cells_used_depth] = None;
                        cubes_ordering.set(new_coord, NOT_INSERTED);
                    });
                }
            }
        }
    });

    num_solutions.load(atomic::Ordering::SeqCst)
}

fn do_depth_first_search_serial(
    cubes_to_develop: &mut [Option<Coord3D>],
    cubes_used: &mut Vec3D<bool>,
    num_cells_used_depth: usize,
    cubes_ordering: &mut Vec3D<i32>,
    min_index_to_use: usize,
    min_rotation_to_use: usize,
) -> u64 {
    let mut num_solutions = 0;

    for cur_ordered_idx_to_use in min_index_to_use..num_cells_used_depth {
        if cubes_to_develop[cur_ordered_idx_to_use].is_none() {
            break;
        }

        for dir_new_cell_add in 0..NUM_NEIGHBORS {
            if cur_ordered_idx_to_use == min_index_to_use && dir_new_cell_add < min_rotation_to_use
            {
                continue;
            }

            let new_i = cubes_to_develop[cur_ordered_idx_to_use].unwrap().x
                + NUDGE_BASED_ON_ROTATION[0][dir_new_cell_add];
            let new_j = cubes_to_develop[cur_ordered_idx_to_use].unwrap().y
                + NUDGE_BASED_ON_ROTATION[1][dir_new_cell_add];
            let new_k = cubes_to_develop[cur_ordered_idx_to_use].unwrap().z
                + NUDGE_BASED_ON_ROTATION[2][dir_new_cell_add];

            let new_coord = c(new_i, new_j, new_k);

            if cubes_used.get(new_coord) {
                continue;
            }

            if !cant_add_cell_because_of_neighbors(
                cubes_to_develop,
                cubes_used,
                cubes_ordering,
                cur_ordered_idx_to_use,
                new_i,
                new_j,
                new_k,
            ) {
                cubes_used.set(new_coord, true);
                cubes_to_develop[num_cells_used_depth] = Some(new_coord);
                cubes_ordering.set(new_coord, num_cells_used_depth as i32);

                if is_first_sight_of_shape(cubes_to_develop, cubes_used, num_cells_used_depth + 1) {
                    num_solutions += if num_cells_used_depth + 1 == cubes_to_develop.len() {
                        1
                    } else {
                        do_depth_first_search_serial(
                            cubes_to_develop,
                            cubes_used,
                            num_cells_used_depth + 1,
                            cubes_ordering,
                            cur_ordered_idx_to_use,
                            dir_new_cell_add,
                        )
                    }
                }

                cubes_used.set(new_coord, false);
                cubes_to_develop[num_cells_used_depth] = None;
                cubes_ordering.set(new_coord, NOT_INSERTED);
            }
        }
    }

    num_solutions
}

fn solve_for_n(n: usize, max_depth_parallel: u8) -> u64 {
    let grid_size = 2 * n + 1 + 2 * BORDER_PADDING;
    assert!(grid_size <= Coord3DElemType::MAX as usize);
    let mut cubes_to_develop: Vec<Option<Coord3D>> = vec![None; n];

    let grid_size_3d = Coord3D::new(
        grid_size as Coord3DElemType,
        grid_size as Coord3DElemType,
        grid_size as Coord3DElemType,
    );
    let mut cubes_used = Vec3D::new(grid_size_3d, false);
    let mut cubes_ordering = Vec3D::new(grid_size_3d, NOT_INSERTED);

    let start_i = (grid_size / 2) as Coord3DElemType;
    let start_j = (grid_size / 2) as Coord3DElemType;
    let start_k = (grid_size / 2) as Coord3DElemType;
    let start_coord = c(start_i, start_j, start_k);

    cubes_used.set(start_coord, true);
    cubes_ordering.set(start_coord, 0);
    cubes_to_develop[0] = Some(start_coord);

    do_depth_first_search(
        &cubes_to_develop,
        &cubes_used,
        1,
        &cubes_ordering,
        0,
        0,
        max_depth_parallel,
    )
}

fn main() {
    let args = Args::parse();
    let n = args.n as usize;
    let num_solutions = solve_for_n(n, args.max_depth_parallel);
    println!("number of polycubes for n={}: {}", n, num_solutions);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solve_for_n_works_for_small_values() {
        for max_depth_parallel in 0..5 {
            assert_eq!(solve_for_n(2, max_depth_parallel), 1);
            assert_eq!(solve_for_n(3, max_depth_parallel), 2);
            assert_eq!(solve_for_n(4, max_depth_parallel), 8);
            assert_eq!(solve_for_n(5, max_depth_parallel), 29);
            assert_eq!(solve_for_n(6, max_depth_parallel), 166);
            assert_eq!(solve_for_n(7, max_depth_parallel), 1023);
            assert_eq!(solve_for_n(8, max_depth_parallel), 6922);
            assert_eq!(solve_for_n(9, max_depth_parallel), 48311);
            assert_eq!(solve_for_n(10, max_depth_parallel), 346543);
        }
    }
}
